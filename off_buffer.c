#include <stdio.h>	 // printf
#include <stdlib.h>	 // exit
#include <string.h> 	 // memset

#include "off_buffer.h"

int off_buffer_offset = 0;
char *off_buffer;

int off_buffer_adddata(char *data)
{
  int len = 0;
  
  len = strlen(data);
  if (off_buffer_offset + len >= OFF_BUFFER_SIZE)
  {
    // buffer overflow
    return OFF_BUFFER_OVERFLOW;
  }

  // update offset
  off_buffer_offset += len;

  // add data to buffer
  sprintf(off_buffer, "%s%s", off_buffer, data);
  
  return len;
}


int off_buffer_getdata(char *endpoint, char *data)
{
  int datalen, endpointlen;
  char *pch;
  
  if (strlen(off_buffer) == 0)
  {
    // buffer empty
    return OFF_BUFFER_IS_EMPTY;
  }
  
  endpointlen = strlen(endpoint);
  pch = strstr(off_buffer, endpoint);
  
  if (pch == NULL)
  {
    // endpoint not found
    return OFF_BUFFER_NOT_FOUND;
  }
  
  datalen = (int)(pch - off_buffer);
  datalen += endpointlen;
  
  // set return value
  strncpy(data, off_buffer, datalen);
  
  // remove return data from buffer
  sprintf(off_buffer, "%s", pch + endpointlen);

  // update offset
  off_buffer_offset -= datalen;
  
  return datalen;
}


int off_buffer_is_buffer_empty(void)
{
  
  if (strlen(off_buffer) == 0)
  {
    // buffer empty
    return 1;
  }
  
  return 0;
}



void off_buffer_erase(void)
{
  memset((char *)off_buffer, 0, OFF_BUFFER_SIZE);  
}

void off_buffer_setup(void)
{
  off_buffer = (char *)malloc(sizeof(char) * OFF_BUFFER_SIZE);
  memset((char *)off_buffer, 0, OFF_BUFFER_SIZE);  
}

