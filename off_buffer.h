#ifndef OFF_BUFFER_H

  #define OFF_BUFFER_H

  #define OFF_BUFFER_SIZE 4096
  #define OFF_BUFFER_OVERFLOW -1
  #define OFF_BUFFER_IS_EMPTY 0
  #define OFF_BUFFER_NOT_FOUND -1

  int off_buffer_adddata(char *data);
  int off_buffer_getdata(char *endpoint, char *data);
  int off_buffer_is_buffer_empty(void);
  void off_buffer_erase(void);
  void off_buffer_setup(void);

#endif // OFF_BUFFER_H