#include <stdio.h>	// printf
#include <stdlib.h>	// exit
#include <unistd.h>	// fork, getpid, setsid
#include <sys/stat.h>	// umask
#include <sys/types.h>	// pid_t

#include "daemonize.h"

// syslog is not opened in the daemonize
// routine. Stderr is used for error reporting
// until file descs are closed in the end.
pid_t daemonize(void) 
{
  pid_t pid;

  pid = fork();
  if (pid < 0) 
  {
    perror("on fork while daemonizing");
    exit(EXIT_FAILURE);
  }

  if (pid > 0) 
  {
    // if we're the parent, we will terminate now
    // so that the child will be moved under init
    return pid;
  }

   // create a new session and put us into it
  if (setsid() < 0) 
  {
    perror("on session creation while daemonizing");
    exit(EXIT_FAILURE);
  }

  // change the directory into root.
  if (chdir("/") < 0) 
  {
    perror("on root dir change while daemonizing");
    exit(EXIT_FAILURE);
  }

  // reset umask
  umask(0);

  // close the normal file descs.
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  // child returns pid = 0
  return pid;
}