#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <unistd.h> 

int sockfd;

int main(int argc, char *argv[])
{
  int portno, n;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  char sendmessage[1024];

  int socketerror = 0;
  socklen_t len = sizeof (socketerror);
  int retval;
  
  
  if (argc < 3) 
  {
    fprintf(stderr,"usage %s hostname port\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  portno = atoi(argv[2]);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) 
  {
    perror("ERROR opening socket");
  }

  server = gethostbyname(argv[1]);
  if (server == NULL) 
  {
    fprintf(stderr,"ERROR, no such host\n");
    exit(EXIT_SUCCESS);
  }
		
  memset((char *)&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  memcpy((char *)&serv_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
  serv_addr.sin_port = htons(portno);
  if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
  {
    printf("ERROR connecting\n");
    perror("ERROR connecting");
    return EXIT_FAILURE;
  }
  printf("socket connected\n");

  for(;;)
  {
    memset((char *)sendmessage, 0, sizeof(char) * 1024);
    
    printf("\nEnter message >");
    scanf("%s", sendmessage);
    printf("\n");
    printf("\n");


    if (strlen(sendmessage) > 0)
    {
      printf("sending: %s\n", sendmessage);

      // check socket state
      retval = getsockopt (sockfd, SOL_SOCKET, SO_ERROR, &socketerror, &len );
    
      if (retval == 0)
      {
	// send message
	n = write(sockfd, sendmessage, strlen(sendmessage));
	if (n < 0)
	{
	  printf("ERROR writing to socket\n");
	}
	else
	{
	  memset((char *)sendmessage, 0, sizeof(char) * 1024);
	  read(sockfd, sendmessage, 1024);
	  printf("REPLY:%s\n", sendmessage);
	}
      }
      else
      {
	printf("socket connection is broken :(\n");
	perror("socket connection is broken :(");
	break;
      }
    }
  }
  close(sockfd);
	
  return EXIT_SUCCESS;
}
