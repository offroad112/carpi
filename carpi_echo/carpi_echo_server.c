#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <unistd.h> 

void socket_handler(int newsockfd);

int main(int argc, char *argv[])
{
  int sockfd, newsockfd;
  int portno;
  struct sockaddr_in serv_addr, cli_addr;
  socklen_t clilen;
  pid_t socket_handler_pid;  

  
  
  if (argc < 2) 
  {
    fprintf(stderr,"usage %s port\n", argv[0]);
    exit(EXIT_FAILURE);
  }
	
  portno = atoi(argv[1]);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) 
  {
    perror("ERROR opening socket");
  }

  memset((char *) &serv_addr ,0,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) 
  {
    perror("ERROR on binding");
  }

  // wait for incoming connections and create socket to connections
  for(;;)
  {
    listen(sockfd, 5);

    printf("waiting connections\n");
    clilen = sizeof(cli_addr);
    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0)
    {
      perror("ERROR on accept");		  
    }
    printf("connection accepted\n");

    // create child process
    socket_handler_pid = fork();
    if (socket_handler_pid == 0)
    {
      // Client handler child
      close(sockfd);
      printf("->  socket handler process started\n");
      socket_handler(newsockfd);
      exit(EXIT_SUCCESS);
    }
    else
    {
      //if (DEBUGMODE) printf("Client handler [%ld] starting with socket [%d]\n", (long)client_handler_child_pid, newsockfd);
      // in parent close new socket
      close(newsockfd);
    }
  } // for(;;)

  printf("Ending\n");

  return EXIT_SUCCESS;
}


void socket_handler(int newsockfd)
{
  int n;
  char *message;

  message = (char *)malloc(sizeof(char) * 1024);

  for(;;)
  {
    // reset message
    memset(message, 0, 1024);

    //wait message
    n = read(newsockfd, message, 1024);
    if (n < 0)
    {
      perror("ERROR reading from socket");
    }
    else
    {
      if (strlen(message) == 0)
      {
	// connection broken, kill process
	break;
      }
      printf("received:%s\n", message);

      // send reply
      n = write(newsockfd, message, strlen(message));
    }
  } // for
}
