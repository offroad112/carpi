#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "off_gpio.h"


char *off_gpio_get_error(int errorcode)
{
	char *error;

	error = (char *)malloc(sizeof(char)*128);
	memset(error, 0, 128);

	switch (errorcode)
	{
		case OFF_GPIO_EXPORT_FAILED:
			sprintf(error, "gpio pin export failed.");
			break;
		case OFF_GPIO_UNEXPORT_FAILED:
			sprintf(error, "gpio pin unexport failed.");
			break;
		case OFF_GPIO_DIRECTION_WRITING_ERROR:
			sprintf(error, "gpio opening pin direction file failed.");
			break;
		case OFF_GPIO_SET_DIRECTION_ERROR:
			sprintf(error, "gpio set pin direction failed.");
			break;
		case OFF_GPIO_OPEN_WRITING_VALUE_ERROR:
			sprintf(error, "gpio opening pin value file failed.");
			break;
		case OFF_GPIO_WRITING_VALUE_ERROR:
			sprintf(error, "gpio writing to pin failed.");
			break;
	}

	return error;
}

int off_gpio_initialize_out_pin(int pin)
{
	int i, result = 0;

	// export pin to use
	if (off_gpio_export(pin) == -1)
	{	
		return OFF_GPIO_EXPORT_FAILED;
	}	

	// set GPIO direction (try 5 times with 0,5 sec pause)
	for(i=0; i<5; i++)
	{
		result = off_gpio_set_pin_direction(pin, OFF_GPIO_OUT);
		if (result != 0)
		{	
			usleep(500 * 1000);
		}
		else
		{
			break;
		}		
	}
	if (result != 0) return result;	

	// test gpio writing and reset pin to low
	for(i=0; i<5; i++)
	{
		result = off_gpio_write(pin, OFF_GPIO_LOW);
		if (result != 0)
		{	
			usleep(500 * 1000);
		}
		else
		{
			break;
		}		
	}
	if (result != 0) return result;	

	return 0;
}

int off_gpio_shutdown_out_pin(int pin)
{
	int i, result = 0;

	// test reset pin to low
	for(i=0; i<5; i++)
	{
		result = off_gpio_write(pin, OFF_GPIO_LOW);
		if (result != 0)
		{	
			usleep(500 * 1000);
		}
		else
		{
			break;
		}
	}
	if (result != 0) return result;	

	// unexport pin to use
	if (off_gpio_unexport(pin) == -1)
	{	
		return OFF_GPIO_UNEXPORT_FAILED;
	}	

	return 0;
}


int off_gpio_set_pin_direction(int pin, int dir)
{
	static const char s_directions_str[]  = "in\0out";

	char path[64];
	int fd;

	snprintf(path, 64, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (fd == -1) 
	{
		return OFF_GPIO_DIRECTION_WRITING_ERROR;
	}
	if (write(fd, &s_directions_str[OFF_GPIO_IN == dir ? 0 : 3], OFF_GPIO_IN == dir ? 2 : 3) == -1)
	{
		return OFF_GPIO_SET_DIRECTION_ERROR;
	}

	close(fd);

	return(0);
}


int off_gpio_write(int pin, int value)
{
	static const char s_values_str[] = "01";
	char path[64];
	int fd;

	snprintf(path, 64, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (fd == -1) 
	{
		return OFF_GPIO_OPEN_WRITING_VALUE_ERROR;
	}

	if (write(fd, &s_values_str[OFF_GPIO_LOW == value ? 0 : 1], 1) != 1) 
	{
		return OFF_GPIO_WRITING_VALUE_ERROR;
	}

	close(fd);

	return(0);
}


int off_gpio_export(int pin)
{
	char buffer[3];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (fd == -1) 
	{
		return OFF_GPIO_EXPORT_FAILED;
	}

	bytes_written = snprintf(buffer, 3, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);

	return(0);
}

int off_gpio_unexport(int pin)
{
	char buffer[3];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (fd == -1) 
	{
		return OFF_GPIO_UNEXPORT_FAILED;
	}

	bytes_written = snprintf(buffer, 3, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);

	return(0);
}

