#ifndef OFF_CONFIG_H

  #define OFF_CONFIG_H

  #define MAX_CONFIG_LINE_LENGTH 256
  
  char *off_readline(char *filepath, int line_number);
  char *off_getvalue(char *filepath, char *section, char *key);
  
  char *off_getparameter(char *filepath, char *section, char *key);
  int off_getparameter_linenumber(char *filepath, char *section, char *key);

  int off_getparametervalue_int(char *parameter, char *key, int *value);
  void off_remove_char_from_string(char c, char *str);
  int off_is_digit(char *str);

#endif // OFF_CONFIG_H