#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>	 // pthread_t
#include <signal.h>

void* heartbeat(void *args);

pthread_t heartbeat_thread;

int sockfd;
char *controller_id;

int heartbeat_status;
int heartbeat_threadstatus;
int heartbeat_interval;

int main(int argc, char *argv[])
{
  int portno, n, wait_reply;
  struct sockaddr_in serv_addr;
  struct hostent *server;
  char pidnum[256];
  char sendmessage[1024];
  pid_t pid;
  char *command, *value, *max, *min;

  int socketerror = 0;
  socklen_t len = sizeof (socketerror);
  int retval;


  if (argc < 3)
  {
    fprintf(stderr,"usage %s hostname port\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  pid = getpid();
  sprintf(pidnum, "%ld:", (long)pid);
  portno = atoi(argv[2]);
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
  {
    perror("ERROR opening socket");
  }

  server = gethostbyname(argv[1]);
  if (server == NULL)
  {
    fprintf(stderr,"ERROR, no such host\n");
    exit(EXIT_SUCCESS);
  }

  memset((char *)&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  memcpy((char *)&serv_addr.sin_addr.s_addr, (char *)server->h_addr, server->h_length);
  serv_addr.sin_port = htons(portno);
  if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
  {
    printf("ERROR connecting\n");
    perror("ERROR connecting");
    return EXIT_FAILURE;
  }
  printf("socket connected\n");

  command = (char *)malloc(sizeof(char) * 128);
  value  = (char *)malloc(sizeof(char) * 128);
  max  = (char *)malloc(sizeof(char) * 128);
  min  = (char *)malloc(sizeof(char) * 128);
  controller_id  = (char *)malloc(sizeof(char) * 128);
  memset((char *)controller_id, 0, sizeof(char) * 128);

  heartbeat_status = 0;
  heartbeat_threadstatus = 0;
  heartbeat_interval = 100;

  for(;;)
  {
    wait_reply = 0;
    memset((char *)command, 0, sizeof(char) * 128);
    memset((char *)value, 0, sizeof(char) * 128);
    memset((char *)max, 0, sizeof(char) * 128);
    memset((char *)min, 0, sizeof(char) * 128);
    memset((char *)sendmessage, 0, sizeof(char) * 1024);

    printf("\nEnter command:\n");
    printf("c id     - {\"connect\":\"id\"}  NOTE: 'pid' uses current process id\n");
    printf("dc id    - {\"disconnect\":\"id\"}\n");
    printf("g m      - {\"get\":\"mode\", \"id\":\"id\"}\n");
    printf("i r      - {\"init\":\"reset\", \"id\":\"id\"}\n");
    printf("i k      - {\"init\":\"keep\", \"id\":\"id\"}\n");
    printf("m t      - {\"mode\":\"training\", \"id\":\"id\"}\n");
    printf("m r      - {\"mode\":\"race\", \"id\":\"id\"}\n");
    printf("m c      - {\"mode\":\"custom\", \"id\":\"id\", \"min_throttle\":min_pros, \"max_throttele\":max_pros}\n");
    printf("e on     - {\"engine\":\"on\", \"id\":\"id\"}\n");
    printf("e off    - {\"engine\":\"off\", \"id\":\"id\"}\n");
    printf("s value  - {\"steering\":value, \"id\":\"id\"}\n");
    printf("t value  - {\"throttle\":value, \"id\":\"id\"}\n");
    printf("h on     - set heartbeat sending on\n");
    printf("h off    - set heartbeat sending off\n");
    printf("hi value - set heartbeat interval (ms)\n");
    printf("k 9      - kills testbench\n");
    printf("\n>");
    scanf("%s %s", command, value);
    printf("\n");
    printf("\n");

    if (strcmp(command, "c") == 0)
    {
      if (strcmp(value, "pid") == 0) sprintf(value, "%ld", (long)pid);

      if (strlen(controller_id) == 0)
      {
      	sprintf(controller_id, "%s", value);
      }
      sprintf(sendmessage, "{\"connect\":\"%s\"}", value);
      wait_reply = 1;
    }
    else if (strcmp(command, "dc") == 0)
    {
      if (strcmp(value, "pid") == 0) sprintf(value, "%ld", (long)pid);
      if (strcmp(value, controller_id) == 0)
      {
	      // reset controller_id
	      memset((char *)controller_id, 0, sizeof(char) * 128);
      }
      sprintf(sendmessage, "{\"disconnect\":\"%s\"}", value);
      wait_reply = 1;
    }
    else if (strcmp(command, "g") == 0)
    {
      if (strcmp(value, "m") == 0)
      {
	      sprintf(value, "mode");
      }
      sprintf(sendmessage, "{\"get\":\"%s\", \"id\":\"%s\"}", value, controller_id);
      wait_reply = 1;
    }
    else if (strcmp(command, "i") == 0)
    {
      if (strcmp(value, "r") == 0)
      {
	      sprintf(value, "reset");
      }
      else if (strcmp(value, "k") == 0)
      {
	      sprintf(value, "keep");
      }
      sprintf(sendmessage, "{\"init\":\"%s\", \"id\":\"%s\"}", value, controller_id);
      wait_reply = 1;
    }
    else if (strcmp(command, "m") == 0)
    {
      if (strcmp(value, "t") == 0)
      {
      	sprintf(value, "training");
      	sprintf(sendmessage, "{\"mode\":\"%s\", \"id\":\"%s\"}", value, controller_id);
      }
      else if (strcmp(value, "r") == 0) 
      {
      	sprintf(value, "race");
      	sprintf(sendmessage, "{\"mode\":\"%s\", \"id\":\"%s\"}", value, controller_id);
      }
      else if (strcmp(value, "c") == 0) 
      {
	      sprintf(value, "custom");
	      printf("\nmin max>");
	      scanf("%s %s", min, max);
	      sprintf(sendmessage, "{\"mode\":\"%s\", \"id\":\"%s\", \"min_throttle\":%s, \"max_throttele\":%s}", value, controller_id, min, max);
      }
      wait_reply = 1;
    }
    else if (strcmp(command, "e") == 0)
    {
      if (strcmp(value, "on") == 0)
      {
	      heartbeat_status = 1;
	      if (heartbeat_threadstatus == 0)
	      {
	        // start heartbeat thread
	        pthread_create(&heartbeat_thread, NULL, heartbeat, NULL);
	      }
      }
      else if (strcmp(value, "off") == 0)
      {
      	heartbeat_status = 0;
      }
      sprintf(sendmessage, "{\"engine\":\"%s\", \"id\":\"%s\"}", value, controller_id);
      wait_reply = 0;
    }
    else if (strcmp(command, "s") == 0)
    {
      sprintf(sendmessage, "{\"steering\":\"%s\", \"id\":\"%s\"}", value, controller_id);
    }
    else if (strcmp(command, "t") == 0)
    {
      sprintf(sendmessage, "{\"throttle\":\"%s\", \"id\":\"%s\"}", value, controller_id);
    }
    else if (strcmp(command, "h") == 0)
    {
      if (strcmp(value, "on") == 0)
      {
	      heartbeat_status = 1;
	      if (heartbeat_threadstatus == 0)
	      {
	        // start heartbeat thread
	        pthread_create(&heartbeat_thread, NULL, heartbeat, NULL);
	      }
      }
      else if (strcmp(value, "off") == 0)
      {
	      heartbeat_status = 0;
      }
    }
    else if (strcmp(command, "hi") == 0)
    {
      heartbeat_interval = atoi(value);
    }
    else if (strcmp(command, "k") == 0)
    {
      // -> kill process
      break;
    }


    if (strlen(sendmessage) > 0)
    {
      printf("sending: %s\n", sendmessage);

      // check socket state
      retval = getsockopt (sockfd, SOL_SOCKET, SO_ERROR, &socketerror, &len );

      if (retval == 0)
      {
      	// send message
      	n = write(sockfd, sendmessage, strlen(sendmessage));
      	if (n < 0)
      	{
	        printf("ERROR writing to socket\n");
	        perror("ERROR writing to socket");
	      }
	      else if (wait_reply)
	      {
	        memset((char *)sendmessage, 0, sizeof(char) * 1024);
	        read(sockfd, sendmessage, 1024);
	        printf("REPLY:%s\n", sendmessage);
	      }
      }
      else
      {
      	printf("socket connection is broken :(");
      	break;
      }
    }
  }
  close(sockfd);

  printf("Carpi testbench [%ld] is going to die\n", (long)pid);
  return EXIT_SUCCESS;
}


void* heartbeat(void *args)
{
  int n, socketerror = 0;
  socklen_t len = sizeof (socketerror);
  int retval;
  char sendmessage[1024];

  memset((char *)sendmessage, 0, sizeof(char) * 1024);
  sprintf(sendmessage, "{\"heartbeat\":true, \"id\":\"%s\"}", controller_id);
  heartbeat_threadstatus = 1;
  //printf("heartbeat loop starts\n");

  for(;;)
  {
    if (heartbeat_status)
    {
      // check socket state
      retval = getsockopt (sockfd, SOL_SOCKET, SO_ERROR, &socketerror, &len );
      if (retval != 0)
      {
      	printf("socket lost, unable to send heartbeat");
	      heartbeat_threadstatus = 0;
	      // kill thread
	      break;
      }

      // send heartbeat
      n = write(sockfd, sendmessage, strlen(sendmessage));
      if (n < 0)
      {
	      printf("ERROR writing to socket\n");
	      perror("ERROR writing to socket");
      }
    }

    usleep(heartbeat_interval * 1000);
  }

  printf("heartbeat loop ends\n");
  heartbeat_threadstatus = 0;

  return NULL;
}

