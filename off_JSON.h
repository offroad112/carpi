#ifndef OFF_JSON_H

  #define OFF_JSON_H

  struct KEYVALUE 
  {
    char *key;
    char *value;
  };

  #define MAX_JSON_LENGTH 1024
  #define MAX_JSON_VALUE_LENGTH 128

  int off_get_keyvalue(char *json, int start_pos, struct KEYVALUE *kv);
  void off_get_value_by_key(char *json, char *key, struct KEYVALUE *kv);
  int off_find_next_char_from_string(char c, char *str);

#endif // OFF_JSON_H