#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "off_JSON.h"


// return value is end of given key value pair
int off_get_keyvalue(char *json, int start_pos, struct KEYVALUE *kv)
{
  char *pch1, *pch2;
  int pos1, pos2, offset;

  memset((char *)kv->key, 0, MAX_JSON_VALUE_LENGTH);
  memset((char *)kv->value, 0, MAX_JSON_VALUE_LENGTH);
  
  //printf("start from %d and total length is %d\n", start_pos, (int)strlen(json));
  
  if (start_pos >= strlen(json) - 1)
  {
    // no key value pair
    return strlen(json) - 1;
  }
  
  // get key to string
  pch1 = strstr(json+start_pos, "\"");
  //printf("pch1:%d\n", (int)strlen(pch1));

  pch2 = strstr(pch1+1, "\""); 
  //printf("pch2:%d\n", (int)strlen(pch2));
  
  strncpy(kv->key,pch1+1,pch2-pch1-1);
  //printf("Key=[%s]\n", kv->key);
    
  // get value to string
  if (pch2[1] == ':')
  {
    // printf(": found\n");
      
    if(pch2[2] == '\"')
    {
      //printf("quotation mark found\n");
      offset = 3;
      pos1 = off_find_next_char_from_string('\"', pch2+offset);
    }
    else
    {
      // no quotation mark
      //printf("value has no quotation marks\n");
      offset = 2;
      pos2 = off_find_next_char_from_string(',', pch2+offset);
      pos1 = off_find_next_char_from_string('}', pch2+offset);

      if ((pos2 > 0) && (pos1 > pos2))
      {
	// , before } -> use pos1
	pos1 = pos2;
      }
    }
    strncpy(kv->value, pch2+offset, pos1-1);    
    //printf("Value=[%s]\n", kv->value);      
  }
  //printf("%d\n", (int)((pch2+offset+pos1)-json));
  
  return (int)((pch2+offset+pos1)-json);
}


void off_get_value_by_key(char *json, char *key, struct KEYVALUE *kv)
{
  char *tempkey;
  int len;
  char *pch1;
  int pos1, pos2, offset;

  tempkey = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);
  memset((char *)kv->key, 0, MAX_JSON_VALUE_LENGTH);
  memset((char *)kv->value, 0, MAX_JSON_VALUE_LENGTH);
  
  // find key
  sprintf(tempkey, "\"%s\":", key);
  len = strlen(tempkey);
  pch1 = strstr(json, tempkey);
  
  if (pch1 == NULL)
  {
    free(tempkey);
    return;
  }
  
  // get value to string
  if(pch1[len] == '\"')
  {
    //printf("quotation mark found\n");
    offset = 1;
    pos1 = off_find_next_char_from_string('\"', pch1+len+offset);
  }
  else
  {
    // no quotation mark
    //printf("value has no quotation marks\n");
    offset = 0;
    pos2 = off_find_next_char_from_string(',', pch1+len+offset);
    pos1 = off_find_next_char_from_string('}', pch1+len+offset);

    if ((pos2 > 0) && (pos1 > pos2))
    {
      // , before } -> use pos1
      pos1 = pos2;
    }
  }
  
  strncpy(kv->value, pch1+len+offset, pos1-1);    
  //printf("Value=[%s]\n", kv->value);

  sprintf(kv->key, "%s", key);
  free(tempkey);
}

int off_find_next_char_from_string(char c, char *str)
{
    int i=0;
    int len = strlen(str)+1;

    for(i=0; i<len; i++)
    {
        if(str[i] == c)
        {
	  return i+1;
        }
    }
    
    return 0;
}
