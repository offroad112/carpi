#include <ctype.h>	      // isdigit
#include <stdio.h>	      // printf
#include <stdlib.h>	      // exit
#include <syslog.h>	      // syslog
#include <signal.h>       // sigfillset
#include <string.h> 	    // memset
#include <fcntl.h>        // open
#include <sys/types.h>	  // pid_t
#include <unistd.h>	      // fork
#include <sys/socket.h>	  // accept
#include <sys/msg.h>	    // msgrcv
#include <sys/ipc.h>	    // ftok
#include <sys/time.h>	    // timeval
#include <netinet/in.h>   // sockaddr_in
#include <pthread.h>	    // pthread_t

#define LOG_DAEMON_NAME	"carpiDaemon"
#define LOG_FACILITY LOG_DAEMON
#define LOG_PRIORITY LOG_ERR

#include "daemonize.h"
#include "off_config.h"
#include "read_carpi_config.h"
#include "off_JSON.h"
#include "off_buffer.h"
#include "off_gpio.h"

#define DEBUGMODE 1
#define HEARTBEAT_PRINT_INTERVAL 600 // default interval (100ms) is takes one minute

#define MODE_TRAINING 1
#define MODE_RACE 2
#define MODE_CUSTOM 3

#define STATE_CONNECTION 0
#define STATE_INITIALIZATION 1
#define STATE_CONTROL_DATA 2

#define HEARTBEAT_LED_PIN 4 // P1-07
#define STEERING_PIN 17		 	// P1-11
#define THROTTLE_PIN 27     // Pi-13

#define STEERING_CHANNEL 0
#define THROTTLE_CHANNEL 1

struct CARPI_CONFIG *conf;
struct timeval last_heartbeat;
struct timeval last_s_pwm_signal;
struct timeval last_t_pwm_signal;
int heartbeat_flag;
pthread_t steering_thread, throttle_thread, heartbeat_led_thread;

int carpi_mode;
int engine_status;
int steering_value;
int throttle_value;
int throttle_max;
int throttle_min;
int heartbeat_lost_s;
int heartbeat_lost_t;

int heartbeat_count;


void socket_handler(int newsockfd, char *active_controller_id);

int state_initialization(int sockfd, char *message, struct KEYVALUE *kv);
int state_control_data(int sockfd, struct KEYVALUE *kv);

void *steering_pwm(void *args);
void *throttle_pwm(void *args);

int convert_to_servoblaster(int value);
void servoblaster_write(int channel, int value);

void set_last_heartbeat(void);
int get_heartbeat_status();

void send_pwm_pulses(int pin, int usec_value, int pulse_count);
void resetSettings(void);
void *heartbeat_led_blinker(void *args);

int main(int argc, char *argv[])
{
  char cwd[1012];
  char *conffile;
  int parameter_cnt = 0, retval;

  int sockfd, newsockfd;
  struct sockaddr_in serv_addr, cli_addr;
  socklen_t clilen;
  pid_t socket_handler_pid;
  char *active_controller_id;

  // setup system settings
  conf = (struct CARPI_CONFIG *)malloc(sizeof(struct CARPI_CONFIG));
  getcwd(cwd, sizeof(cwd));
  conffile = (char *)malloc(sizeof(char) * 1024);
  sprintf(conffile, "%s/carpi.conf", cwd);
  printf("config file [%s]\n", conffile);
  parameter_cnt = read_carpi_config(conffile, conf);
  printf("%d default values read from configuration file.\n", parameter_cnt);

  // setup system defaults
  active_controller_id = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);
  memset((char *)active_controller_id, 0, MAX_JSON_VALUE_LENGTH);
  heartbeat_flag = 0;

	// setup gpio pins
	retval = off_gpio_initialize_out_pin(HEARTBEAT_LED_PIN);
	if (retval != 0)
	{
		printf("Failed to initialize heartbeat led pin. (%s)\n", off_gpio_get_error(retval));
		return retval;
	}


	// start heartbeat led thread
	pthread_create(&heartbeat_led_thread, NULL, heartbeat_led_blinker, NULL);


  if (DEBUGMODE) printf("\nstarting parent work\n");

  // setup server socket
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
  {
    perror("ERROR opening socket");
  }

  memset((char *) &serv_addr ,0,sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(conf->port);
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
  {
    perror("ERROR on binding");
  }

  // wait for incoming connections and create socket to connections
  for(;;)
  {
    listen(sockfd, 5);

    // TEMP in future we could allow multiple controllers
    if (strlen(active_controller_id) == 0)
    {
      if (DEBUGMODE) printf("waiting connections\n");
      clilen = sizeof(cli_addr);
      newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
      if (newsockfd < 0)
      {
				perror("ERROR on accept");		  
      }
      if (DEBUGMODE) printf("connection accepted\n");

      // create child process
      socket_handler_pid = fork();
      if (socket_handler_pid == 0)
      {
				// Client handler child
				close(sockfd);
        printf("->  socket handler process started\n");
				socket_handler(newsockfd, active_controller_id);
				exit(EXIT_SUCCESS);
      }
      else
      {
				//if (DEBUGMODE) printf("Client handler [%ld] starting with socket [%d]\n", (long)client_handler_child_pid, newsockfd);
				// in parent close new socket
				close(newsockfd);
      }
    }
  } // for(;;)


	// Disable and reset GPIO pins
	retval = off_gpio_shutdown_out_pin(HEARTBEAT_LED_PIN);
	if (retval != 0)
	{ 
		printf("Failed to unexport heartbeat led pin.\n");
	}

	retval = off_gpio_shutdown_out_pin(STEERING_PIN);
	if (retval != 0)
	{ 
		printf("Failed to unexport steering pwm pin.\n");
	}

	retval = off_gpio_shutdown_out_pin(THROTTLE_PIN);
	if (retval != 0)
	{ 
		printf("Failed to unexport throttle pwm pin.\n");
	}


  if (DEBUGMODE) printf("Ending\n");

  return EXIT_SUCCESS;
}



void socket_handler(int newsockfd, char *active_controller_id)
{
  int n, val;
  char *message;
  int state = 0;
  struct KEYVALUE *kv, *kv_id;
  int heartbeat_msg, force_heartbeat_log = 0;

  //
  // STATES
  //
  // NO LOCKED ID (state = 0)
  // - handle only CONNECTION
  // - CONNECTION ID -> INITIALIZATION
  //
  // INITIALIZATION (state = 1)
  // - handle GET, INIT, MODE, ENGINE, DISCONNECT (with locked id)
  // - ENGINE ON -> CONTROL DATA
  // - DISCONNECT -> NO LOCKED ID
  //
  // CONTROL DATA (state = 2)
  // - handle HEARTBEAT, STEERING, THROTTLE, ENGINE (with locked id)
  // - ENGINE OFF -> INITIALIZATION
  //
  kv = (struct KEYVALUE *)malloc(sizeof(struct KEYVALUE));
  kv->key = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);
  kv->value = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);
  
  kv_id = (struct KEYVALUE *)malloc(sizeof(struct KEYVALUE));
  kv_id->key = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);
  kv_id->value = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);

  message = (char *)malloc(sizeof(char) * MAX_JSON_LENGTH);
  
  syslog(LOG_PRIORITY, "Carpi connected and waiting messages.\n");
  if (DEBUGMODE) printf("Carpi connected and waiting messages.\n");
  
  // setup buffer to read JSON-messages
  off_buffer_setup();
  
  for(;;)
  {
    // reset message
    memset(message, 0, MAX_JSON_LENGTH);
    
    if (off_buffer_is_buffer_empty())
    {
      // buffer is empty, read from socket
      //wait message
      n = read(newsockfd, message, MAX_JSON_LENGTH);
      if (n > 0) 
      { 
	      val = off_buffer_adddata(message);
	      //if (DEBUGMODE) printf("added:%d\n", val);
	      if (val == OFF_BUFFER_OVERFLOW)
	      {
	        syslog(LOG_PRIORITY, "Carpi JSON-buffer overflow! Resetting buffer.\n");
	        if (DEBUGMODE) printf("Carpi JSON-buffer overflow! Resetting buffer.\n");
	        off_buffer_erase();
	      }
	      else
	      {
	        // read one message from buffer
	        n = off_buffer_getdata("}", message);
	      }
      }
    }
    else
    {
      if (DEBUGMODE) printf("reading data from buffer without reading socket :-)\n");
      
      // read one message from buffer
      n = off_buffer_getdata("}", message);
    }

    //if (DEBUGMODE) printf("acid:%p kv->value:%p\n", active_controller_id, kv->value);
        
    if (n < 0) 
    { 
      perror("ERROR reading from socket or buffer");
    }
    else
    {
      //if (DEBUGMODE) printf("[%s] - %d\n", message, (int)strlen(message));
      if (strlen(message) == 0)
      {
				// kill socket handler
				state = 0;
				memset((char *)active_controller_id, 0, MAX_JSON_VALUE_LENGTH);
				if (DEBUGMODE) printf("\n\nwaiting new connection\n");
				return;
      }
      
      // read command
      off_get_keyvalue(message, 0, kv);
      
      heartbeat_msg = 0;
      if (strcmp(kv->key, "heartbeat") == 0) heartbeat_msg = 1;
      if (force_heartbeat_log) heartbeat_msg = 0;
      
      if ((DEBUGMODE) && (!heartbeat_msg)) printf("\nreceived: %s\n", message);
      if (!heartbeat_msg) syslog(LOG_PRIORITY, "received: %s\n", message);
      //if ((DEBUGMODE) && (!heartbeat_msg)) printf("kv: %s:%s\n", kv->key, kv->value);
      
      if ((state == 0) && (strcmp(kv->key, "connect") == 0))
      {
				sprintf(active_controller_id, "%s", kv->value);
				resetSettings();
				state = 1;
				syslog(LOG_PRIORITY, "connected id=%s\n", active_controller_id);
				if (DEBUGMODE) printf("connected=%s\n", active_controller_id);
	
				// send reply
				memset(message, 0, MAX_JSON_LENGTH);
				sprintf(message, "{\"connect\":\"ok\"}");
				n = write(newsockfd, message, strlen(message));
				//if (DEBUGMODE) printf("reply send, return %d\n", n);
      }
      else if ((state == 1) && (strcmp(kv->key, "disconnect") == 0) && (strcmp(kv->value, active_controller_id) == 0))
      {
				syslog(LOG_PRIORITY, "disconnected id=%s\n", active_controller_id);
				if (DEBUGMODE) printf("disconnected=%s\n", active_controller_id);
				memset((char *)active_controller_id, 0, MAX_JSON_VALUE_LENGTH);
				state = 0;
	
				// send reply
				memset(message, 0, MAX_JSON_LENGTH);
				sprintf(message, "{\"disconnect\":\"%s\"}", active_controller_id);
				n = write(newsockfd, message, strlen(message));

				// kill socket handler
				return;
      }
      else
      {
				// get id
				off_get_value_by_key(message, "id", kv_id);
				//if ((DEBUGMODE) && (!heartbeat_msg)) printf("kv_id: %s:[%s]\n", kv_id->key, kv_id->value);
				//if ((DEBUGMODE) && (!heartbeat_msg)) printf("active_controller_id: [%s]\n", active_controller_id);
				if ((state > 0) && (strcmp(active_controller_id, kv_id->value) != 0))
				{
		  		// wrong controller_id, reject package
				  syslog(LOG_PRIORITY, "wrong controller id [%s], package rejected.\n", kv->value);
				  if (DEBUGMODE) printf("wrong controller id [%s], package rejected.\n", kv->value);
				}
				else
				{
          switch (state)
	  			{
	    			case 1:
	      			state = state_initialization(newsockfd, message, kv);
	      			break;
	    
				    case 2:
				      // control date stage
				      state = state_control_data(newsockfd, kv);
				      break;
				  } // switch (state)
				} // id check
      } // is it connect?
    } // read data success
  } // for
}


int state_initialization(int sockfd, char *message, struct KEYVALUE *kv)
{
  struct KEYVALUE *kv_tmp;
  int min_true, max_true, min, max;
  
  if (strcmp(kv->key, "get") == 0)
  {
    if (strcmp(kv->value, "mode") == 0)
    {
	    // send reply
	    memset(message, 0, MAX_JSON_LENGTH);
			switch (carpi_mode)
			{
				case MODE_TRAINING:
			    sprintf(message, "{\"mode\":\"training\", \"min_throttle\":%d, \"max_throttele\":%d}", throttle_min, throttle_max);
					break;
				case MODE_RACE:
			    sprintf(message, "{\"mode\":\"race\", \"min_throttle\":%d, \"max_throttele\":%d}", throttle_min, throttle_max);
					break;
				case MODE_CUSTOM:
			    sprintf(message, "{\"mode\":\"custom\", \"min_throttle\":%d, \"max_throttele\":%d}", throttle_min, throttle_max);
					break;
			}
	    write(sockfd, message, strlen(message));
	    if (DEBUGMODE) printf("get mode done '%s'\n", message);
		}
	}
  else if (strcmp(kv->key, "init") == 0)
  {
    // neutralize steering and throttle (keep and reset)
    steering_value = 500;
    throttle_value = 500;	      

    if (strcmp(kv->value, "reset") == 0)
    {
      carpi_mode = MODE_TRAINING;
      throttle_min = 30;
      throttle_max = 30;
    }
    
    // send reply
    memset(message, 0, MAX_JSON_LENGTH);
    sprintf(message, "{\"init\":\"%s\"}", kv->value);
    write(sockfd, message, strlen(message));
    
    if (DEBUGMODE) printf("initialization '%s' done\n", kv->value);
  }
  else if (strcmp(kv->key, "mode") == 0)
  {
    if (strcmp(kv->value, "training") == 0)
    {
      carpi_mode = MODE_TRAINING;
      throttle_min = 30;
      throttle_max = 30;
      if (DEBUGMODE) printf("training mode setup done\n");
    }
    else if (strcmp(kv->value, "race") == 0)
    {
      carpi_mode = MODE_RACE;
      throttle_min = 100;
      throttle_max = 100;
      if (DEBUGMODE) printf("race mode setup done\n");
    }
    else if (strcmp(kv->value, "custom") == 0)
    {
      kv_tmp = (struct KEYVALUE *)malloc(sizeof(struct KEYVALUE));
      kv_tmp->key = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);
      kv_tmp->value = (char *)malloc(sizeof(char) * MAX_JSON_VALUE_LENGTH);

      off_get_value_by_key(message, "min_throttle", kv_tmp);
      min_true = off_is_digit(kv_tmp->value);
      if (min_true) min = atoi(kv_tmp->value);
      off_get_value_by_key(message, "max_throttle", kv_tmp);
      max_true = off_is_digit(kv_tmp->value);
      if (max_true) max = atoi(kv_tmp->value);

      // set mode if all values are acceptable
      if ((min_true) && (max_true) && (min > 0) && (min <= 101) && (max > 0) && (max <= 101))
      {
	      carpi_mode = MODE_CUSTOM;
	      throttle_min = min;
	      throttle_max = max;
	      if (DEBUGMODE) printf("custom mode setup done\n");
      }
    }

    // send reply
    memset(message, 0, MAX_JSON_LENGTH);
    sprintf(message, "{\"mode\":\"%s\"}", kv->value);
    write(sockfd, message, strlen(message));
  }
  else if (strcmp(kv->key, "engine") == 0)
  {
    if (strcmp(kv->value, "on") == 0)
    {
      // start your engines
      set_last_heartbeat();
      engine_status = 1;
      heartbeat_count = 0;
      
      // start control threads
      pthread_create(&steering_thread, NULL, steering_pwm, NULL);
      pthread_create(&throttle_thread, NULL, throttle_pwm, NULL);

      if (DEBUGMODE) printf("engine started\n");
      
      return STATE_CONTROL_DATA;
    }

    // send reply
    memset(message, 0, MAX_JSON_LENGTH);
    sprintf(message, "{\"engine\":\"on\"}");
    write(sockfd, message, strlen(message));
  }

  return STATE_INITIALIZATION;
}

int state_control_data(int sockfd, struct KEYVALUE *kv)
{
  int value_true, value;
  double tmp;
  
  // CONTROL DATA STATE
  if (strcmp(kv->key, "heartbeat") == 0)
  {
    set_last_heartbeat();
  }
  else if (strcmp(kv->key, "steering") == 0)
  {
    value_true = off_is_digit(kv->value);
    if (value_true) value = atoi(kv->value);
    if ((value_true) && (value >= 0) && (value <= 1000))
    {
      steering_value = value; 
      if (DEBUGMODE) printf("steering set to %d\n", value);
    }
  }
  else if (strcmp(kv->key, "throttle") == 0)
  {
    value_true = off_is_digit(kv->value);
    if (value_true) value = atoi(kv->value);
    if ((value_true) && (value >= 0) && (value <= 1000))
    {
      if (value > 500)
      {
      	// forward
	      value -= 500;
	      tmp = (double)throttle_max / (double)100.0;
	      tmp = tmp * value;
	      value = (int)tmp;
	      value += 500;
      }
      else if (value < 500)
      {
	      // backward
	      value = 500 - value;					// example: 100 (means 80% throttle backwards) converted to 400 (80% forward)
	      tmp = (double)throttle_min / (double)100.0;		// throttle_min = 50% -> converts 400 to 200 (40 % forward)
	      tmp = tmp * value;
	      value = (int)tmp;
	      value = 500 - value;					// final value is 300 (40% backwards)
      }
      throttle_value = value; 
      if (DEBUGMODE) printf("throttle set to %d (real value)\n", value);
    }
  }
  else if (strcmp(kv->key, "engine") == 0)
  {
    if (strcmp(kv->value, "off") == 0)
    {
      engine_status = 0;
      heartbeat_flag = 0;

      // send reply
      write(sockfd, "{\"engine\":\"off\"}", strlen("{\"engine\":\"off\"}"));

      if (DEBUGMODE) printf("engine stopped\n");
      
      return STATE_INITIALIZATION;
    }
  }

  return STATE_CONTROL_DATA;  
}



void* steering_pwm(void *args)
{
  int last_steering_value = -1;
  long diff;
  struct timeval now;

  for(;;)
  {
    // thread should send one pulse at least once on 250 ms
    gettimeofday(&now, NULL);
    diff = (now.tv_sec  - last_t_pwm_signal.tv_sec) * 1000.0;
    diff += (now.tv_usec  - last_t_pwm_signal.tv_usec) / 1000.0;
    
    if (diff > 200)
    {
      // force pwm pulse sending
      last_steering_value = -1;
    }

    // is engine turn off
    if (engine_status == 0)
    {
      // engine is off, neutralize steering
      servoblaster_write(STEERING_CHANNEL, convert_to_servoblaster(500));
      // shutdown thread
      break;
    }
    else
    {
      // engine still running
      
      // check heart beat
      if (get_heartbeat_status() == 0)
      {
	      // neutralize steering
	      if ((DEBUGMODE) && (heartbeat_lost_s == 0)) printf("heartbeat lost, neutralizing steering\n");	
	      heartbeat_lost_s = 1;
	      steering_value = 500;
      }
      else
      {
	      heartbeat_lost_s = 0;
      }

      if (steering_value != last_steering_value)
      {
        // send steering pulse
        servoblaster_write(STEERING_CHANNEL, convert_to_servoblaster(steering_value));
        last_steering_value = steering_value;
        gettimeofday(&last_s_pwm_signal, NULL);
      }
    } 
    usleep(1000); 
  } // for
  
  return NULL;
}


void* throttle_pwm(void *args)
{
  int last_throttle_value = -1;
  long diff;
  struct timeval now;

  for(;;)
  {
    // thread should send one pulse at least once on 250 ms
    gettimeofday(&now, NULL);
    diff = (now.tv_sec  - last_t_pwm_signal.tv_sec) * 1000.0;
    diff += (now.tv_usec  - last_t_pwm_signal.tv_usec) / 1000.0;
    
    if (diff > 200)
    {
      // force pwm pulse sending
      last_throttle_value = -1;
    }

    // is engine turn off
    if (engine_status == 0)
    {
      // engine is off, neutralize throttle
      servoblaster_write(THROTTLE_CHANNEL, convert_to_servoblaster(500));
      
      // shutdown thread
      break;
    }
    else
    {
      // engine still running
      
      // check heart beat
      if (get_heartbeat_status() == 0)
      {
      	// neutralize throttle
	      if ((DEBUGMODE) && (heartbeat_lost_t == 0)) printf("heartbeat lost, neutralizing throttle\n");
	      heartbeat_lost_t = 1;
	      throttle_value = 500;
      }
      else
      {
      	heartbeat_lost_t = 0;
      }

      if (throttle_value != last_throttle_value)
      {
        // send throttle pulse
        servoblaster_write(THROTTLE_CHANNEL, convert_to_servoblaster(throttle_value));
        last_throttle_value = throttle_value;
        gettimeofday(&last_t_pwm_signal, NULL);
      }
    } 
    usleep(1000); 
  } // for
  
  return NULL;
}


int convert_to_servoblaster(int value)
{
  double tmp;
  
  value += 1000;
  tmp = (double)value / (double)10.0;

  return (int)tmp;
}

void servoblaster_write(int channel, int value)
{
	char buffer[12];
	ssize_t bytes_written;
	int fd;

	fd = open("/dev/servoblaster", O_WRONLY);
	if (fd == -1) 
	{
		return;
	}
  memset(buffer, 0, 12);
	bytes_written = snprintf(buffer, 12, "%d=%d\n", channel, value);
  //printf("%s", buffer);
	write(fd, buffer, bytes_written);
	close(fd);

	return;
}

void set_last_heartbeat(void)
{
  struct timeval now;
  
  gettimeofday(&now, NULL);
  last_heartbeat = now;
  
  heartbeat_flag = 1;
  heartbeat_count++;
  if (heartbeat_count > HEARTBEAT_PRINT_INTERVAL)
  {
    heartbeat_count = 0;
    if (DEBUGMODE) printf("%d heartbeats received\n", HEARTBEAT_PRINT_INTERVAL);
  }
  //if (DEBUGMODE) printf("last heartbeat [%ld][%ld]\n", last_heartbeat.tv_sec, last_heartbeat.tv_usec);  
}


int get_heartbeat_status()
{
  long diff;
  struct timeval now;
  
  gettimeofday(&now, NULL);
  
  if (heartbeat_flag)
  {
    diff = (now.tv_sec  - last_heartbeat.tv_sec) * 1000.0;
    diff += (now.tv_usec  - last_heartbeat.tv_usec) / 1000.0;
    
    if (diff > conf->emergency_shutdown_time)
    {
      if (DEBUGMODE) printf("heartbeat fails, heartbeat delay %ld ms\n", diff);
      heartbeat_flag = 0;
    }
  }
  
  return heartbeat_flag;
}


//
// usec_value is 0-1000 microseconds
//
void send_pwm_pulses(int pin, int usec_value, int pulse_count)
{
  int i=0;
  
  for(i=0; i<pulse_count;i++)
  {
    // uptime 1000-2000 us
		off_gpio_write(pin, OFF_GPIO_HIGH);
    usleep(1000 + usec_value); 

    // down time 18000-19000us
		off_gpio_write(pin, OFF_GPIO_LOW);
    usleep(19000 - usec_value); 
  }
}


void resetSettings(void)
{
  engine_status = 0; 			// off
  steering_value = 500;		// neutral
  throttle_value = 500;		// neutral

  carpi_mode = 1;  				// training
  throttle_max = 30;			// 30%
  throttle_min = 30;			// 30%
  
  heartbeat_lost_s = 0;		// heartbeat steering ok
  heartbeat_lost_t = 0;		// heartbeat throttle ok
}


void *heartbeat_led_blinker(void *args)
{
	int interval = 400, offset, uptime;
	char sequence[616] = "- .... . -.--  - .... .. -. -.-  --- ..- .-.  .... . .- -.. ...  .- .-. .  .. -.  - .... . .. .-.  .... .- -. -.. ...  -... ..- -  ...- .. --- .-.. . -. -  ..- ... .  -... .-. .. -. --. ...  ...- .. --- .-.. . -. -  .--. .-.. .- -. ...  -.- . . .--.  .... .. --  - .. . -.. --..--  .. -  -- .- -.- . ...  .... .. --  .-- . .-.. .-..  .... . .----. ...  --. . - - .. -. --.  -... . - - . .-. --..--  -.-. .- -. .----. -  -.-- --- ..-  - . .-.. .-.. ..--..  -. ---  -- --- .-. .  -.-. .- -.  - .... . -.--  -.- . . .--.  ..- ...  .. -.  .-.. .. ... - . -.  -.. .- -- -.  .. -  --..-- .-- .  .-- .. .-.. .-..  .-- .. -.";

	for(;;)
	{
		for(offset=0; offset<616; offset++)
		{
			if (sequence[offset] == '.')
			{
				uptime = (int)((double)interval * 0.25);
			}
			else if (sequence[offset] == '-')
			{
				uptime = (int)((double)interval * 0.50);
			}
			else if (sequence[offset] == ' ')
			{
				uptime = 0;
			}
			
			//printf("%d: pulse:%d - %d\n", offset, uptime, (interval - uptime));
			if (uptime > 0)
			{
				off_gpio_write(HEARTBEAT_LED_PIN, OFF_GPIO_HIGH);
				usleep(uptime * 1000);
			}	

			off_gpio_write(HEARTBEAT_LED_PIN, OFF_GPIO_LOW);
			usleep((interval - uptime) * 1000);
		}
	}

}

