#ifndef OFF_GPIO_H

  #define OFF_GPIO_H

	#define OFF_GPIO_IN  0
	#define OFF_GPIO_OUT 1

	#define OFF_GPIO_LOW  0
	#define OFF_GPIO_HIGH 1

	// define error codes
	#define OFF_GPIO_EXPORT_FAILED -1
	#define OFF_GPIO_UNEXPORT_FAILED -2
	#define OFF_GPIO_DIRECTION_WRITING_ERROR -3
	#define OFF_GPIO_SET_DIRECTION_ERROR -4
	#define OFF_GPIO_OPEN_WRITING_VALUE_ERROR -5
	#define OFF_GPIO_WRITING_VALUE_ERROR -6

	char *off_gpio_get_error(int errorcode);
	int off_gpio_initialize_out_pin(int pin);
	int off_gpio_shutdown_out_pin(int pin);
	int off_gpio_set_direction_n_times(int pin, int dir, int n, int interval_ms);
	int off_gpio_set_pin_direction(int pin, int dir);
	int off_gpio_test_write_n_times(int pin, int value, int n, int interval_ms);
	int off_gpio_write(int pin, int value);
	int off_gpio_export(int pin);
	int off_gpio_unexport(int pin);

#endif // OFF_GPIO_H
