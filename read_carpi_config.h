#ifndef READ_CARPI_CONFIG_H

  #define READ_CARPI_CONFIG_H

  struct CARPI_CONFIG 
  {
    int port;
    int emergency_shutdown_time;
  }; 
  
  int read_carpi_config(char *filepath, struct CARPI_CONFIG *conf);

#endif // READ_CARPI_CONFIG_H
  