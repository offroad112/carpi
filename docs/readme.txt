Carpin osat ja käyttö


ServoBlaster
------------
Carpi käyttää pwm-signaalin generointiin ServoBlaster dma-kirjastoa PiBits-projektista.  

PiBits-projekti:
    git clone http://github.com/richardghirst/PiBits

käännä user space servod
    cd PiBits/ServoBlaster/user
    make servod

käynnistä ohjelma seuraavilla asetuksilla
    sudo ./servod --p1pins=11,13 --idle-timeout=400ms

carpi käyttää P1-11 pinniä ohjauksen pwm lähtönä ja P1-13 kaasun lähtönä. Idle-time-asetuksella varaudutaan siihen, että mikäli ohjelma kaatuu tai menee hallitsemattomaan tilaan, niin ServoBlaster pysäyttää auton lopettamalla pwm:n generoinnin.



carpi-projekti
--------------

carpi-projekti:
    git clone http://bitbucket.org/offroad112/carpi


carpi
-----
Itse pääohjelma, joka huolehtii viestien välityksestä ohjaimen kanssa ja ohjaa viestien perusteella autoa. Viestit välitetään JSON-muodossa wlanin yli. 

Carpi muodostaa wlan access pointin, johon ohjain liittyy. Wlan access point on luotu http://elinux.org/RPI-Wireless-Hotspot dokumentin mukaan.

Carpi avaa socket yhteyden ja jää odottamaan yhteyden ottoa. Oletus portti on 10042. Se on määriteltävissä carpi.conf-tiedostosta. Yhteyden muodostamisen jälkeen ohjain lukitaan id:llä, jotta mahdollisten muiden ohjainten viestejä ei sekoiteta. Carpi sallii tällä hetkellä vain yhden ohjainen käytön kerrallaan. Ohjaimen annettua disconnect-komento carpi jää odottamaan uutta ohjaimen yhteyden ottoa.

Yhteyskatkosten (heartbeat signaali katkeaa CONTROL DATA-tilassa) kohdalla carpi pysäyttää auton ja neutralisoi ohjauksen. Carpi jää odottamaan seuraavaa heartbeattia ja sellaisen saadessaan ottaa auton uudelleen kontrolliinsa.

Ohjelman sisäisen tilakoneen tilat ja käsiteltävät viestit:
  //
  // STATES
  //
  // NO LOCKED ID (state = 0)
  // - handle only CONNECTION
  // - CONNECTION ID -> INITIALIZATION
  //
  // INITIALIZATION (state = 1)
  // - handle GET, INIT, MODE, ENGINE, DISCONNECT (with locked id)
  // - ENGINE ON -> CONTROL DATA
  // - DISCONNECT -> NO LOCKED ID
  //
  // CONTROL DATA (state = 2)
  // - handle HEARTBEAT, STEERING, THROTTLE, ENGINE (with locked id)
  // - ENGINE OFF -> INITIALIZATION
  //


    
testbench
---------
Testiohjelma jolla voidaan lähettää carpille käsin viestejä.

  $ ./testbench localhost 10042
  socket connected

  Enter command:
  c id     - {"connect":"id"}  NOTE: 'pid' uses current process id
  dc id    - {"disconnect":"id"}
  g m      - {"get":"mode", "id":"id"}
  i r      - {"init":"reset", "id":"id"}
  i k      - {"init":"keep", "id":"id"}
  m t      - {"mode":"training", "id":"id"}
  m r      - {"mode":"race", "id":"id"}
  m c      - {"mode":"custom", "id":"id", "min_throttle":min_pros, "max_throttele":max_pros}
  e on     - {"engine":"on", "id":"id"}
  e off    - {"engine":"off", "id":"id"}
  s value  - {"steering":value, "id":"id"}
  t value  - {"throttle":value, "id":"id"}
  h on     - set heartbeat sending on
  h off    - set heartbeat sending off
  hi value - set heartbeat interval (ms)
  k 9      - kills testbench
  >_

Esimerkki viestiketju:
c id  - lukitsee ohjaimen "id", siirtää ohjelman INITIALIZATION-tilaan
i r   - resetoi asetukset oletuksiin
e on  - siirtää tilan CONTROL DATA-tilaan
t 900 - asettaa kaasun tilan 0-1000, 500 - neutraali
s 200 - asettaa ohjauksen tilan 0-1000, 500 - suoraan eteen
t... * n
s... * n
e off - pysäyttää auton ja siirtää tilan takaisin INITIALIZATION-tilaan
dc id - vapauttaa ohjaimen ja siirtää caprin odottamaan uusia ohjaimia

heartbeat-komennoilla voi simuloida yhteyskatkoja ja hidasta yhteyttä.



carpi_echo_server
-----------------
Yksinkertainen echo serveri, joka odottaa socket-yhteyttä ja saadessaan sockettiin viestin echottaa sen takaisin socketiin. Ohjelmalla voi helposti testata yhteyden viestin välitystä.

  $ carpi_echo_server 10042



carpi_echo_client
-----------------
Yksinkertainen client, jolla voi lähettää sockettiin vapaamuotoisia viestejä.

  $ carpi_echo_client localhost 10042



