#ifndef DAEMONIZE_H

  #define DAEMONIZE_H

  pid_t daemonize(void);

#endif // DAEMONIZE_H