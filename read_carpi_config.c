#include <stdio.h>
#include <stdlib.h>	// malloc

#include "off_config.h"
#include "read_carpi_config.h"

int read_carpi_config(char *filepath, struct CARPI_CONFIG *conf)
{
  int cnt = 0;
  char *parameter;

  parameter = (char *)malloc(sizeof(char) * 128);
  parameter = off_getparameter(filepath, "carpi_startup_settings", "port");
  if (parameter != NULL)
  {
    if (off_getparametervalue_int(parameter, "port", &conf->port))
    {
      cnt++;
    }
  }
  // printf("[%d, %s]\n", (int)strlen(parameter), parameter);

  parameter = off_getparameter(filepath, "carpi_startup_settings", "emergency_shutdown_time");
  if (parameter != NULL)
  {
    if (off_getparametervalue_int(parameter, "emergency_shutdown_time", &conf->emergency_shutdown_time))
    {
      cnt++;
    }
  }
  // printf("[%d, %s]\n", (int)strlen(parameter), parameter);

  return cnt;
}
