#include <ctype.h>	// isdigit
#include <stdio.h>	// printf
#include <stdlib.h>	// exit
#include <string.h> 	// memset

#include "off_config.h"

char *off_readline(char *filepath, int line_number)
{
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int cnt = 1;

  fp = fopen(filepath, "r");
  if (fp == NULL)
  {
    return NULL;
  }

  while ((read = getline(&line, &len, fp)) != -1)
  {
    //printf("Retrieved line of length %zu :\n", read);
    //printf("%s", line);

    if (cnt++ == line_number)
    {
      fclose(fp);
      return line;
    }
  }
  fclose(fp);

  if (line)
  {
    free(line);
  }

  return NULL;
}


char *off_getvalue(char *filepath, char *section, char *key)
{
  char *pch;
  char *parameter;

  pch = (char *)malloc(sizeof(char) * MAX_CONFIG_LINE_LENGTH);
  parameter = (char *)malloc(sizeof(char) * MAX_CONFIG_LINE_LENGTH);

  parameter = off_getparameter(filepath, section, key);
  if (parameter != NULL)
  {
    // get and return value
    pch = strstr(parameter, "=");
    sprintf(parameter, "%s", pch+1);
    return parameter;
  }

  if (parameter)
  {
    free(parameter);
  }

  return NULL;
}



char *off_getparameter(char *filepath, char *section, char *key)
{
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int section_found = 0;
  char *akey;

  akey = (char *)malloc(sizeof(char) * 512);
  memset(akey, 0, 512);

  line = (char *)malloc(sizeof(char) * MAX_CONFIG_LINE_LENGTH);

  sprintf(akey, "%s=", key);

  fp = fopen(filepath, "r");
  if (fp == NULL)
  {
    return NULL;
  }

  while ((read = getline(&line, &len, fp)) != -1)
  {
    //printf("Retrieved line of length %zu : line:%s\n", read, line);

    if (!section_found)
    {
      if (strstr(line, section) > 0)
      {
	section_found = 1;
      }
    }
    else
    {
      if (strstr(line, "[") > 0)
      {
	// new section starts before key was found
	fclose(fp);
	return NULL;
      }
      else if (strstr(line, akey) > 0)
      {
	// return line
	fclose(fp);
	off_remove_char_from_string('\n', line);
	return line;
      }
    }
  }
  fclose(fp);

  if (line)
  {
    free(line);
  }

  return NULL;
}


int off_getparameter_linenumber(char *filepath, char *section, char *key)
{
  FILE *fp;
  char *line;
  size_t len = 0;
  ssize_t read;
  int section_found = 0, lineno = 0;
  char akey[sizeof(key)+1];

  line = (char *)malloc(sizeof(char) * MAX_CONFIG_LINE_LENGTH);

  sprintf(akey, "%s=", key);

  fp = fopen(filepath, "r");
  if (fp == NULL)
  {
    return 0;
  }

  while ((read = getline(&line, &len, fp)) != -1) 
  {
    lineno++;
    //printf("Retrieved line of length %zu :\n", read);
    //printf("%s", line);
    if (!section_found)
    {
      if (strstr(line, section) > 0)
      {
	section_found = 1;
      }
    }
    else
    {
      if (strstr(line, "[") > 0)
      {
	// new section starts before key was found
	fclose(fp);
	return 0;
      }
      else if (strstr(line, akey) > 0)
      {
	// return line number
	fclose(fp);
	return lineno;
      }
    }
  }
  fclose(fp);

  if (line)
  {
    free(line);
  }

  return 0;
}


// returns 0 on error and 1 on success
int off_getparametervalue_int(char *parameter, char *key, int *value)
{
  int i, idigit=1;
  char *pch;
  char tkey[128];
  char sval[256];

  if ((int)strlen(key) > 127)
  {
    // too long key
    return 0;
  }

  sprintf(tkey, "%s=", key);
  if (strstr(parameter, tkey) > 0)
  {
    // get value to string
    pch = strstr(parameter, "=");
    memset(sval, 0, sizeof(sval));
    sprintf(sval, "%s", pch+1);
    //printf("len [%d, '%s']\n", (int)strlen(sval), sval);

    // check input and get int value
    for(i=0; i<(int)strlen(sval);i++)
    {
      if (!isdigit(sval[i]))
      {
	idigit = 0;
	break;
      }
    }

    if (idigit)
    {
      *value = atoi(sval);

      // success
      return 1;
    }
    else
    {
      // there is other characters than numbers in input
      return 0;
    }
  }

  // key not found
  return 0;
}


void off_remove_char_from_string(char c, char *str)
{
    int i=0;
    int len = strlen(str)+1;

    for(i=0; i<len; i++)
    {
        if(str[i] == c)
        {
            // Move all the char following the char "c" by one to the left.
            strncpy(&str[i],&str[i+1],len-i);
        }
    }
}


int off_is_digit(char *str)
{
  int i, idigit = 1;

  // check input and get int value
  for(i=0; i<(int)strlen(str);i++)
  {
    if (!isdigit(str[i]))
    {
      idigit = 0;
      break;
    }
  }

  return idigit;
}
